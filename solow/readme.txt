An elementary example of a deterministic economic simulation.

solowSim.gss contains two functions:
  solowSteady() takes some Solow growth model parameters and Returns a matrix
    containing the steady-state levels of some per-effective-labor-unit
    macroeconomic variables. Returns these values in a single matrix.
  
  solowPaths() takes the same parameters as well as some initial conditions
    and a number of periods to simulate, then computes growth paths for
    aggregate, per-capita, and per-effective-labor-unit variables, as well
    as the factor price growth paths.

example.gss simply demonstrates the use of the function.